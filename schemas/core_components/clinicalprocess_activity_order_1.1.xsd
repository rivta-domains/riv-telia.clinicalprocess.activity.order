<?xml version="1.0" encoding="UTF-8" ?>
<!-- 
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements. See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership. Inera licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License. You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied. See the License for the
 specific language governing permissions and limitations
 under the License.
 -->
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:tns="urn:riv:clinicalprocess:activity:order:1"
  xmlns:m1="urn:riv:clinicalprocess:activity:order:1.1"
  xmlns:codes="urn:riv:clinicalprocess:activity:order:enum:1.1"
  targetNamespace="urn:riv:clinicalprocess:activity:order:1" elementFormDefault="qualified"
  attributeFormDefault="unqualified" version="1.1">
  <xs:import namespace="urn:riv:clinicalprocess:activity:order:enum:1.1"
    schemaLocation="clinicalprocess_activity_order_enum_1.1.xsd"/>
  <xs:import namespace="urn:riv:clinicalprocess:activity:order:1.1"
    schemaLocation="clinicalprocess_activity_order_1.1_ext.xsd" />

	<xs:complexType name="ActivityOrderType">
    <xs:sequence>
      <xs:element name="id" type="tns:IIType"/>
      <xs:element name="careGiverId" type="tns:IIType"/>
      <xs:element name="careUnitId" type="tns:IIType"/>
      <xs:element name="observationRequest" type="tns:ObservationRequestType" maxOccurs="unbounded"/>
      <xs:element name="status" type="codes:ActivityOrderStatusEnum"/>
      <xs:element name="sourceSystemHSAId" type="tns:IIType"/>
      <xs:element name="typeOfTransfer" type="codes:TypeOfTransferEnum"/>
      <xs:element name="comment" type="xs:string" minOccurs="0"/>
      <xs:element name="signDateTime" type="tns:TimeStampType" minOccurs="0"/>
      <xs:element name="registerDateTime" type="tns:TimeStampType" minOccurs="0"/>
      <xs:element name="iCalendar" type="xs:string" minOccurs="0"/>
      <xs:element name="careProcessId" type="tns:UUIDType" minOccurs="0"/>
      <xs:element name="emailAddress" type="xs:string" minOccurs="0"/>
      <xs:element name="mobileNumber" type="xs:string" minOccurs="0"/>
      <xs:element minOccurs="0" name="device" type="tns:DeviceType"/>
      <xs:element minOccurs="1" name="patient" type="tns:PatientType"/>
      <xs:element minOccurs="1" name="requester" type="tns:ParticipantType" maxOccurs="1"/>
      <xs:element name="performer" type="tns:ParticipantType"/>
      <!-- nya attribut --> 
      <xs:element ref="m1:sourceSystemName" minOccurs="0"/>
      <xs:element ref="m1:deliveryAddress" minOccurs="0"/>
      <xs:element ref="m1:informationPolicy" minOccurs="0"/>
      <xs:element ref="m1:receiver" minOccurs="0" maxOccurs="unbounded"/>
      <!-- xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded"/ -->
    </xs:sequence>
  </xs:complexType>

  <xs:simpleType name="UUIDType">
    <xs:restriction base="xs:string">
      <xs:length value="36" fixed="true"/>
      <xs:pattern
        value="[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-4[0-9a-fA-F]{3}-[8-9a-bA-B][0-9a-fA-F]{3}-[0-9a-fA-F]{12}"
      />
    </xs:restriction>
  </xs:simpleType>

  <xs:complexType name="CVType">
    <xs:annotation>
      <xs:documentation/>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="code" type="xs:string"/>
      <xs:element name="codeSystem" type="xs:string"/>
      <xs:element name="codeSystemName" type="xs:string" minOccurs="0"/>
      <xs:element name="codeSystemVersion" type="xs:string" minOccurs="0"/>
      <xs:element name="displayName" type="xs:string" minOccurs="0"/>
      <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="DeviceType">
    <xs:sequence>
      <xs:element name="id" type="tns:IIType" minOccurs="0"/>
      <xs:element name="type" type="tns:CVType" minOccurs="0"/>
      <xs:element name="model" type="tns:SCType" minOccurs="0"/>
      <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="ObservationRequestType">
    <xs:sequence>
      <xs:element name="type" type="tns:CVType"/>
      <xs:element name="value" type="tns:PQType" minOccurs="0"/>
      <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="IIType">
    <xs:sequence>
      <xs:element name="root" type="xs:string"/>
      <xs:element name="extension" type="xs:string"/>
      <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="ParticipantType">
    <xs:sequence>
      <xs:element name="patient" type="tns:PatientType" minOccurs="0"/>
      <xs:element name="healthcareProfessional" type="tns:HealthCareProfessionalType" minOccurs="0"/>
      <xs:element name="organisation" type="tns:OrganisationType" minOccurs="0"/>
      <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded"/>
	</xs:sequence>
  </xs:complexType>

  <xs:complexType name="HealthCareProfessionalType">
    <xs:sequence>
      <xs:element name="id" type="tns:IIType" minOccurs="0"/>
      <xs:element name="name" type="xs:string" minOccurs="0"/>
      <xs:element name="organisation" type="tns:OrganisationType" maxOccurs="unbounded"/>
      <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="OrganisationType">
    <xs:sequence>
      <xs:element name="id" type="tns:IIType"/>
      <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="PatientType">
    <xs:sequence>
      <xs:element name="patientId" type="tns:IIType"/>
      <xs:element name="consent" type="xs:boolean" minOccurs="0"/>
      <!-- nya attribut -->
      <xs:element ref="m1:name" minOccurs="0"/>
      <xs:element ref="m1:address" minOccurs="0"/>
      <xs:element ref="m1:mobileNumber" minOccurs="0"/>
      <xs:element ref="m1:emailAddress" minOccurs="0"/>
      <xs:element ref="m1:nonDecisionMaking" minOccurs="0"/>
      <xs:element ref="m1:nextOfKin" minOccurs="0"/>
      <!-- xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded"/ -->
    </xs:sequence>
  </xs:complexType>

  <xs:simpleType name="TimeStampType">
    <xs:annotation>
      <xs:documentation>
        A quantity specifying a point on the axis of natural time.
        A point in time is most often represented as a calendar
        expression.

        The time has the format YYYYMMDDhhmmss
      </xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:pattern
        value="(19|20)\d\d(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(0[0-9]|1[0-9]|2[0123])([0-5])([0-9])([0-5])([0-9])"
      />
    </xs:restriction>
  </xs:simpleType>

  <xs:complexType name="SCType">
    <xs:sequence>
      <xs:element name="code" type="tns:CVType" minOccurs="0"/>
      <xs:element name="value" type="xs:string" minOccurs="0"/>
      <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="PQType">
    <xs:sequence>
      <xs:element name="value" type="xs:double"/>
      <xs:element name="unit" type="xs:string"/>
      <!-- lax tillagt i 1.1 -->
      <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="ResultType">
    <xs:annotation>
      <xs:documentation> </xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="resultCode" type="codes:ResultCodeEnum"/>
      <xs:element name="errorCode" type="codes:ErrorCodeEnum" minOccurs="0"/>
      <xs:element name="logId" type="xs:string"/>
      <xs:element minOccurs="0" name="subCode" type="xs:string"/>
      <xs:element name="message" type="xs:string" minOccurs="0"/>
      <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>

  </xs:schema>